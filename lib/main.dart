import 'package:flutter/material.dart';
import 'package:tip_calculator/home.dart';

void main() {
  runApp(TipCalculator());
}

class TipCalculator extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BillSplitter(title: 'BillSplitter'),
    );
  }
}
