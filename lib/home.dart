import 'package:flutter/material.dart';

class BillSplitter extends StatefulWidget {
  BillSplitter({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _BillSplitterState createState() => _BillSplitterState();
}

class _BillSplitterState extends State<BillSplitter> {
  int _tipPercentage = 0;
  int _personCounter = 1;
  double _billAmount = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1),
        alignment: Alignment.center,
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.all(20),
          scrollDirection: Axis.vertical,
          children: [
            Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                color: Colors.greenAccent,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Total per person',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 20.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(
                        '£ ${calculatePerPerson( _billAmount, _personCounter, _tipPercentage).toStringAsFixed(2)}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 37.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // SECOND CONTAINER HERE
            Container(
              margin: EdgeInsets.only(top: 20.0),
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(
                  color: Colors.blueGrey.shade100,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: [
                  Text('£'),
                  TextField(
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                    decoration: InputDecoration(
                      labelStyle: TextStyle(
                        color: Colors.green,
                      ),
                      labelText: 'Bill Amount',
                      prefixIcon: Icon(Icons.attach_money),
                    ),
                    onChanged: (String value) {
                      try {
                        _billAmount = double.parse(value);
                      } catch (e) {
                        _billAmount = 0.0;
                      }
                    },
                  ),
                  // SPLIT ROW
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Split',
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      Row(
                        children: [
                          splitManipulator('-'),
                          Text(
                            '$_personCounter',
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                              fontSize: 20.0,
                            ),
                          ),
                          splitManipulator('+'),
                        ],
                      ),
                    ],
                  ),
                  // TIP ROW
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Tip',
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Text(
                          '£ ${calculateTotalTip(_billAmount, _personCounter, _tipPercentage)}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0),
                        ),
                      ),
                    ],
                  ),
                  // SLIDER
                  Column(
                    children: [
                      Text(
                        '$_tipPercentage%',
                        style: TextStyle(
                          color: Colors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Slider(
                          min: 0.0,
                          max: 100.0,
                          activeColor: Colors.green,
                          inactiveColor: Colors.grey,
                          divisions: 20,
                          value: _tipPercentage.toDouble(),
                          onChanged: (double newValue) {
                            setState(() {
                              _tipPercentage = newValue.round();
                            });
                          }),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  InkWell splitManipulator(String plusMinus) {
    return InkWell(
      onTap: () {
        setState(() {
          if (plusMinus == '-' && _personCounter > 1) {
            _personCounter--;
          }
          if (plusMinus == '+') {
            _personCounter++;
          }
        });
      },
      child: Container(
        width: 40.0,
        height: 40.0,
        margin: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7.0),
          color: Colors.greenAccent.shade100,
        ),
        child: Center(
          child: Text(
            plusMinus,
            style: TextStyle(
              color: Colors.green,
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );
  }

  double calculatePerPerson(double billAmount,  int splitBy,int tipPercentage) {
    double perPerson = 0.0;
    if (billAmount < 0.0 || splitBy < 0) {
    } else {
      perPerson =
          (calculateTotalTip(billAmount, splitBy, tipPercentage) + billAmount) /
              splitBy;
    }

    return perPerson;
  }

  double calculateTotalTip(double billAmount, int splitBy, int tipPercentage) {
    print('billAmount = $billAmount');
    print('tipPercentage = $tipPercentage');
    print('splitBy = $splitBy');
    double totalTip = 0.0;
    if (billAmount < 0 || billAmount.toString().isEmpty || billAmount == null) {
      // oops! nothing to do lol
    } else {
      totalTip = (billAmount * tipPercentage) / 100;
    }
    return totalTip;
  }
}
